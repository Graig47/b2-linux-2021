# TP1 : (re)Familiaration avec un système GNU/Linux

## Sommaire

- [TP1 : (re)Familiaration avec un système GNU/Linux](#tp1--refamiliaration-avec-un-système-gnulinux)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)
  - [II. Partitionnement](#ii-partitionnement)
    - [1. Préparation de la VM](#1-préparation-de-la-vm)
    - [2. Partitionnement](#2-partitionnement)
  - [III. Gestion de services](#iii-gestion-de-services)
  - [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
  - [2. Création de service](#2-création-de-service)
    - [A. Unité simpliste](#a-unité-simpliste)
    - [B. Modification de l'unité](#b-modification-de-lunité)

## 0. Préparation de la machine

Détails de la VM dans VBox (les deux sont configurées de façon similaire) :

![Détails de la première VM dans VBox](./pic/vm_details.png)


🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**
  - voir screen VBox plus haut pour la carte NAT

```bash
# Infos sur la carte NAT
[it4@node1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:23:9e:5a brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 86305sec preferred_lft 86305sec
    inet6 fe80::a00:27ff:fe23:9e5a/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
[...]

# Route par défaut
[it4@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
10.0.2.0/24 dev enp0s3 proto kernel scope link src 10.0.2.15 metric 100
10.1.101.0/24 dev enp0s8 proto kernel scope link src 10.1.101.11 metric 101

# Ping fonctionnel vers un serveur connu d'internet
[it4@node1 ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=16.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=15.10 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 15.957/16.225/16.494/0.297 ms
```

- **un accès à un réseau local** (les deux machines peuvent se `ping`) (via la carte Host-Only)
  - voir la conf VBox de la VM plus hant, dans le screen VBox

Sur `node1.tp1.linux` :

```bash
# Config IP de la carte host-only
[it4@node1 ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3a:2b:fc brd ff:ff:ff:ff:ff:ff
    inet 10.1.101.11/24 brd 192.168.57.255 scope global dynamic noprefixroute enp0s8
       valid_lft 325sec preferred_lft 325sec
    inet6 fe80::a00:27ff:fe3a:2bfc/64 scope link 
       valid_lft forever preferred_lft forever
```

Sur `node2.tp1.linux` :

```bash
# Config IP de la carte host-only
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3c:ff:7f brd ff:ff:ff:ff:ff:ff
    inet 10.1.101.12/24 brd 192.168.57.255 scope global dynamic noprefixroute enp0s8
       valid_lft 324sec preferred_lft 324sec
    inet6 fe80::a00:27ff:fe3c:ff7f/64 scope link 
       valid_lft forever preferred_lft forever

# Ping fonctionnel vers node1.tp1.linux
[it4@node2 ~]$ ping 10.1.101.11 -c 2
PING 10.1.101.11 (10.1.101.11) 56(84) bytes of data.
64 bytes from 10.1.101.11: icmp_seq=1 ttl=64 time=1.41 ms
64 bytes from 10.1.101.11: icmp_seq=2 ttl=64 time=1.01 ms

--- 10.1.101.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.014/1.212/1.411/0.201 ms
```

- **vous n'utilisez QUE `ssh` pour administrer les machines**

Sur `node1.tp1.linux` :

```bash
# On vérifie que le service sshd est actif
[it4@node1 ~]$ sudo systemctl status sshd
[sudo] password for it4: 
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2021-09-28 11:17:01 CEST; 7min ago

# On vérifie que sshd écoute bien derrière le port 22
[it4@node1 ~]$ sudo ss -alnpt
State            Recv-Q           Send-Q                     Local Address:Port                     Peer Address:Port          Process
LISTEN           0                128                              0.0.0.0:22                            0.0.0.0:*              users:(("sshd",pid=849,fd=5))

# On vérifie que le firewall accepte bien le trafic ssh
[it4@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules:
```

Test de connexion :

```bash
[it4@nowhere]$ ssh 10.1.101.11
it4@10.1.101.11's password: 
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Sep 28 11:18:28 2021 from 192.168.57.1
[it4@node1 ~]$ 
```

- **les machines doivent avoir un nom**

```bash
# Définition du nom de domaine
[it4@node1 ~]$ sudo hostname node1.tp1.b1
[it4@node1 ~]$ echo "node1.tp1.b1" | sudo tee /etc/hostname

# Vérification
[it4@node1 ~]$ hostname
node1.tp1.linux
```

- **utiliser `1.1.1.1` comme serveur DNS**
  - vérifier avec le bon fonctionnement avec la commande `dig`
    - avec `dig`, demander une résolution du nom `ynov.com`
    - mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé
    - mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu

```bash
# Modification de la conf d'interface pour ajouter le DNS
[it4@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
DNS1=1.1.1.1

# On recharge la conf
[it4@node1 ~]$ sudo nmcli con reload
[it4@node1 ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path: /org/freedesktop/NetworkManager/ActiveConnection/3)

# Vérification que la conf a changé
[it4@node1 ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search home tp1.linux
nameserver 192.168.1.1
nameserver 1.1.1.1

# Test avec dig
[it4@node1 ~]$ dig google.com @1.1.1.1

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com @1.1.1.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 50770
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.			IN	A

;; ANSWER SECTION:
google.com.		197	IN	A	216.58.209.238

;; Query time: 17 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Tue Sep 28 15:35:18 CEST 2021
;; MSG SIZE  rcvd: 55
```

La réponse dans le `dig` :
```bash
google.com.             197     IN      A       216.58.209.238
```

L'IP du serveur qui a répondu :

```bash
;; SERVER: 1.1.1.1#53(1.1.1.1)
```

- **les machines doivent pouvoir se joindre par leurs noms respectifs**

node1 :

```bash
[it4@node1 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.1.101.11 node1.tp1.b2 node1
10.1.101.12 node2.tp1.b2 node2

[it4@node1 ~]$ ping -c 2 node2
PING node2.tp1.b2 (10.1.101.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.1.101.12): icmp_seq=1 ttl=64 time=0.698 ms
64 bytes from node2.tp1.b2 (10.1.101.12): icmp_seq=2 ttl=64 time=0.875 ms

--- node2.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1059ms
rtt min/avg/max/mdev = 0.698/0.786/0.875/0.092 ms
```

node2 :

```bash
[it4@node2 ~]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

10.1.101.11 node1.tp1.b2 node1
10.1.101.12 node2.tp1.b2 node2
[it4@node2 ~]$ ping -c 2 node1
PING node1.tp1.b2 (10.1.101.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.1.101.11): icmp_seq=1 ttl=64 time=0.749 ms
64 bytes from node1.tp1.b2 (10.1.101.11): icmp_seq=2 ttl=64 time=0.945 ms

--- node1.tp1.b2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 0.749/0.847/0.945/0.098 ms
```

- **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**

```bash
# On check l'état du firewall
[it4@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

# Le service cockpit n'est pas utilisé, ni le dhcpv6, on les enlève
[it4@node1 ~]$ sudo firewall-cmd --remove-service dhcpv6-client --permanent
success
[it4@node1 ~]$ sudo firewall-cmd --remove-service cockpit --permanent
success

# Reload de la conf
[it4@node1 ~]$ sudo firewall-cmd --reload
success

# Vérif
[it4@node1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: ssh
  ports: 
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```

## I. Utilisateurs

[Une section dédiée aux utilisateurs est dispo dans le mémo Linux.](../../cours/memo/commandes.md#gestion-dutilisateurs).

### 1. Création et configuration

🌞 Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :

```bash
# Ajout du user
[it4@node1 ~]$ sudo adduser marcel -m -d /home/marcel -s /bin/bash
# Définition d'un mot de passe pour le nouveau user
[it4@node1 ~]$ sudo passwd marcel
New password: 
BAD PASSWORD: The password is shorter than 8 characters
Retype new password: 
passwd: all authentication tokens updated successfully.

# Test
[it4@node1 ~]$ su - marcel
Password: 

[marcel@node1 ~]$ pwd
/home/marcel

[marcel@node1 ~]$ echo $SHELL
/bin/bash
```

🌞 Créer un nouveau groupe `admins` qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.

🌞 Ajouter votre utilisateur à ce groupe `admins`.

```bash
# Création du groupe admins
[it4@node1 ~]$ sudo groupadd admins

# Modification de la conf sudo
[it4@node1 ~]$ sudo visudo
[it4@node1 ~]$ sudo cat /etc/sudoers | grep admin
%admins	ALL=(ALL)	ALL

# Ajout de marcel au groupe admins
[it4@node1 ~]$ sudo usermod -aG admins marcel

# Test
[it4@node1 ~]$ su - marcel
Password: 
Last login: Tue Sep 28 16:18:28 CEST 2021 on pts/0
[marcel@node1 ~]$ sudo -l
Matching Defaults entries for marcel on node1:
    !visiblepw, always_set_home, match_group_by_gid, always_query_group_plugin, env_reset, env_keep="COLORS DISPLAY HOSTNAME HISTSIZE KDEDIR LS_COLORS",
    env_keep+="MAIL PS1 PS2 QTDIR USERNAME LANG LC_ADDRESS LC_CTYPE", env_keep+="LC_COLLATE LC_IDENTIFICATION LC_MEASUREMENT LC_MESSAGES", env_keep+="LC_MONETARY
    LC_NAME LC_NUMERIC LC_PAPER LC_TELEPHONE", env_keep+="LC_TIME LC_ALL LANGUAGE LINGUAS _XKB_CHARSET XAUTHORITY", secure_path=/sbin\:/bin\:/usr/sbin\:/usr/bin

User marcel may run the following commands on node1:
    (ALL) ALL
```

### 2. SSH

```bash
# Génération d'une paire de clé sur la machine hôte
[it4@nowhere ~]$ ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/home/it4/.ssh/id_rsa): /home/it4/.ssh/id_rsa_tp1_linux_b2
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/it4/.ssh/id_rsa_tp1_linux_b2
Your public key has been saved in /home/it4/.ssh/id_rsa_tp1_linux_b2.pub
[...]

# Dépôt des clés sur les deux machines node1 et node2
# node1
[it4@nowhere]$ ssh-copy-id -f -i ~/.ssh/id_rsa_tp1_linux_b2 10.101.1.11 
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/it4/.ssh/id_rsa_tp1_linux_b2.pub"
it4@10.101.1.11's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh '10.101.1.11'"
and check to make sure that only the key(s) you wanted were added.

# node2
[it4@nowhere]$ ssh-copy-id -f -i ~/.ssh/id_rsa_tp1_linux_b2 10.101.1.12
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/it4/.ssh/id_rsa_tp1_linux_b2.pub"
it4@10.101.1.12's password: 

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh '10.101.1.12'"
and check to make sure that only the key(s) you wanted were added.

# Tests
# node1
[it4@nowhere]$ ssh -i ~/.ssh/id_rsa_tp1_linux_b2 10.101.1.11
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Sep 28 16:23:44 2021 from 10.101.1.1
[it4@node1 ~]$ whoami
it4

# node2
[it4@nowhere]$ ssh -i ~/.ssh/id_rsa_tp1_linux_b2 10.101.1.12
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Sep 28 16:23:45 2021 from 10.101.1.1
[it4@node2 ~]$ whoami
it4
```

## II. Partitionnement

```bash
# On repère le nom des disques de la VM
[it4@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk 
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part 
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk 
sdc           8:32   0    3G  0 disk 
sr0          11:0    1 1024M  0 rom  

# Ajoute des deux disques à la conf LVM (en tat que physical volumes)
[it4@node1 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[it4@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.

# Création du volume group LVM, contenant les deux disques
[it4@node1 ~]$ sudo vgcreate data /dev/sdb /dev/sdc
  Volume group "data" successfully created

# Création des trois logical volumes LVM (partitions)
[it4@node1 ~]$ sudo lvcreate -L 1G --name data1 data
  Logical volume "data1" created.
[it4@node1 ~]$ sudo lvcreate -L 1G --name data2 data
  Logical volume "data2" created.
[it4@node1 ~]$ sudo lvcreate -L 1G --name data3 data
  Logical volume "data3" created.

# Vérification de la conf LVM
[it4@node1 ~]$ sudo pvs
  PV         VG   Fmt  Attr PSize  PFree   
  /dev/sda2  rl   lvm2 a--  <7.00g       0 
  /dev/sdb   data lvm2 a--  <3.00g 1020.00m
  /dev/sdc   data lvm2 a--  <3.00g   <2.00g

[it4@node1 ~]$ sudo vgs
  VG   #PV #LV #SN Attr   VSize  VFree
  data   2   3   0 wz--n-  5.99g 2.99g
  rl     1   2   0 wz--n- <7.00g    0 

[it4@node1 ~]$ sudo lvs
  LV    VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  data1 data -wi-ao----   1.00g                                                    
  data2 data -wi-ao----   1.00g                                                    
  data3 data -wi-ao----   1.00g                                                    
  root  rl   -wi-ao----  <6.20g                                                    
  swap  rl   -wi-ao---- 820.00m

# Formatage des partitions en ext4
[it4@node1 ~]$ sudo mkfs -t ext4 /dev/data/data1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 868636d5-d976-434d-ae65-dc4073c55753
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[it4@node1 ~]$ sudo mkfs -t ext4 /dev/data/data2
[...]

[it4@node1 ~]$ sudo mkfs -t ext4 /dev/data/data3
[...]

# Création des points de montage
[it4@node1 ~]$ sudo mkdir /mnt/part{1..3}

# Montage des partitions
[it4@node1 ~]$ sudo mount /dev/data/data1 /mnt/part1
[it4@node1 ~]$ sudo mount /dev/data/data2 /mnt/part2
[it4@node1 ~]$ sudo mount /dev/data/data3 /mnt/part3

# Vérif du montage
[it4@node1 ~]$ df -h
Filesystem              Size  Used Avail Use% Mounted on
devtmpfs                386M     0  386M   0% /dev
tmpfs                   405M     0  405M   0% /dev/shm
tmpfs                   405M  5.6M  400M   2% /run
tmpfs                   405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root     6.2G  2.0G  4.2G  33% /
/dev/sda1              1014M  182M  833M  18% /boot
tmpfs                    81M     0   81M   0% /run/user/1000
/dev/mapper/data-data1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/data-data2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/data-data3  976M  2.6M  907M   1% /mnt/part3

# Démontage des partitions
[it4@node1 ~]$ sudo umount /mnt/part1
[it4@node1 ~]$ sudo umount /mnt/part2
[it4@node1 ~]$ sudo umount /mnt/part3

# Modification de fstab
[it4@node1 ~]$ sudo vim /etc/fstab
[it4@node1 ~]$ cat /etc/fstab | grep data
/dev/data/data1 /mnt/part1 ext4 defaults 0 0
/dev/data/data2 /mnt/part2 ext4 defaults 0 0
/dev/data/data3 /mnt/part3 ext4 defaults 0 0

# Test que la conf fstab est fonctionnelle
[it4@node1 ~]$ sudo mount -a -v
/                        : ignored
/boot                    : already mounted
none                     : ignored
/mnt/part1               : successfully mounted
/mnt/part2               : successfully mounted
/mnt/part3               : successfully mounted

[it4@node1 ~]$ df -h
Filesystem              Size  Used Avail Use% Mounted on
devtmpfs                386M     0  386M   0% /dev
tmpfs                   405M     0  405M   0% /dev/shm
tmpfs                   405M  5.6M  400M   2% /run
tmpfs                   405M     0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root     6.2G  2.0G  4.2G  33% /
/dev/sda1              1014M  182M  833M  18% /boot
tmpfs                    81M     0   81M   0% /run/user/1000
/dev/mapper/data-data1  976M  2.6M  907M   1% /mnt/part1
/dev/mapper/data-data2  976M  2.6M  907M   1% /mnt/part2
/dev/mapper/data-data3  976M  2.6M  907M   1% /mnt/part3
```

## III. Gestion de services

Vérification que firewall est lancé et activé au démarrage du système :

```bash
[it4@node1 ~]$ systemctl is-active firewalld
active
[it4@node1 ~]$ systemctl is-enabled firewalld
enabled
```

## 2. Création de service

### A. Unité simpliste

```bash
# Création du service web
[it4@node1 ~]$ sudo vim /etc/systemd/system/web.service
[it4@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

# Ouverture du port 8888/tcp
[it4@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[it4@node1 ~]$ sudo firewall-cmd --reload
success

# On relance la conf systemd (pour que notre fichier web.service soit découvert)
[it4@node1 ~]$ sudo systemctl daemon-reload

# Manipulation du service web
[it4@node1 ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[it4@node1 ~]$ sudo systemctl start web
[it4@node1 ~]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.

# Test local
[it4@node1 ~]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
[...]
</html>
```

```bash
# Test depuis node2
[it4@node2 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
[...]
</html>
```

### B. Modification de l'unité

```bash
# Modification de l'unité
[it4@node1 ~]$ sudo vim /etc/systemd/system/web.service
[it4@node1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv

[Install]
WantedBy=multi-user.target

# Création d'un fichier de test
[it4@node1 ~]$ echo "yolo" | sudo tee /srv/yolo
yolo

# On donne l'appartenance de /srv et son contenu à l'utilisateur web
[it4@node1 ~]$ sudo chown web:web -R /srv

# On recharge la conf systemd
[it4@node1 ~]$ sudo systemctl daemon-reload

# Redémarrage du service web
[it4@node1 ~]$ sudo systemctl restart web

# Vérification que le process tourne en tant que l'utilisateur web
[it4@node1 ~]$ ps -ef | grep pytho
root         819       1  0 16:01 ?        00:00:00 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid
root         848       1  0 16:01 ?        00:00:00 /usr/libexec/platform-python -Es /usr/sbin/tuned -l -P
web        24800       1  1 16:43 ?        00:00:00 /bin/python3 -m http.server 8888
it4        24802    1940  0 16:43 pts/0    00:00:00 grep --color=auto pytho

# Test avec curl
[it4@node1 ~]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="yolo">yolo</a></li>
</ul>
<hr>
</body>
</html>
```

Bim.
