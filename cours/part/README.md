# Partitionnement

<!-- vim-markdown-toc GitLab -->

* [Théorie](#théorie)
* [Partitionnement physique](#partitionnement-physique)
* [LVM](#lvm)
* [Formater et monter des partitions](#formater-et-monter-des-partitions)

<!-- vim-markdown-toc -->

# Théorie

Le partitionnement permet de découper des périphériques de stockage en plusieurs parties. 

Il est pertinent de partitionner des périphériques de stockage afin d'héberger différents types de données, qui peuvent avoir des degrés de sensibilité différents, ou encore qui seront plus ou moins souvent accédées.

Sur les OS GNU/Linux, on partitionne souvent le système comme suit : 

| Point de montage | Taille minimale approximative | Utilité                                                                                                   | Caractéristique                                                     |
|------------------|-------------------------------|-----------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------|
| `/var`           | 3Go                           | Environnement d'exécution des applications                                                                | Beaucoup d'écritures                                                |
| `/tmp`           | 1Go                           | Dossier temporaire                                                                                        | Beaucoup d'écritures et de lectures, droits très permissifs (`777`) |
| `/home`          | 1Go                           | Répertoires personnels des utilisateurs                                                                   | Peu de lectures/écritures, données potentiellement sensibles        |
| `/`              | 5Go                           | Racine de l'arborescence : tout ce qui n'est pas explicitement dans une autre partition se retrouvera ici | Lectures/écritures constantes                                       |
| swap             | Même taille que la RAM        | Permet de soulager la RAM                                                                                 | Beaucoup de lectures et écritures                                   |

# Partitionnement physique

> Pas vu en cours.

0. Repérer le(s) disque(s) à partitionner
```bash
$ lsblk
```

1. Partitionner le disque avec un outil comme `fdisk`, `parted` ou autres.

Puis [formater et monter la(les) partition(s).](#formater-et-monter-des-partitions).

# LVM

LVM *(Logical Volume Manager)* est un outil disponible sur les OS GNU/Linux qui permet de partitionner de façon logique les périphériques de stockage.

Le concept est le suivant :
* on ajoute les périphériques de stockage au gestionnaire LVM : ils deviennent des ***PV*** *(Physical Volumes)*
* les PVs peuvent être aggrégés en ***VG*** *(Volume Group)*
* les VGs peuvent alors être divisés en ***LV*** *(Logical Volume)* qui sont des partitions utilisables comme des partitions classiques

0. Repérer les disques à ajouter dans LVM 

```bash
$ lsblk
```

1. Ajouter le(s) disque(s) en tant que PV *(Physical Volume)* dans LVM

```bash
$ sudo pvcreate /dev/<NEW_DISK>

# Par exemple
$ sudo pvcreate /dev/sdb
$ sudo pvcreate /dev/sdc
$ sudo pvcreate /dev/sdd1 # oui, on peut ajouter des disques, ou des partitions physiques à LVM

# Vérif
$ sudo pvs
$ sudo pvdisplay
```

2. Créer un VG *(Volume Group)*

```bash
$ sudo vgcreate <VG_NAME> <FIRST_PV>

# Par exemple
$ sudo vgcreate data /dev/sdb

# Vérif
$ sudo vgs
$ sudo vgdisplay
```

3. Ajouter d'autres PVs au VG nouvellement créé **(facultatif : uniquement si vous avez d'autres PVs à ajouter)**

```bash
$ sudo vgextend <VG_NAME> <PV_PATH>

# Par exemple
$ sudo vgextend data /dev/sdc
$ sudo vgextend data /dev/sdd1

# Vérif
$ sudo vgs
$ sudo vgdisplay
```

4. Créer des LV *(Logical Volumes)*

```bash
$ sudo lvcreate -L <SIZE> <VG_NAME> -n <LV_NAME>

# Par exemple
$ sudo lvcreate -L 10G data -n ma_data_frer
$ sudo lvcreate -L 1500M data -n ta_data_frer
$ sudo lvcreate -l 100%FREE data -n last_data # crée un LV qui occupe la totalité de l'espace restant du Volume Group "data"

# Vérif
$ sudo lvs
$ sudo lvdisplay
```

Un LV **est** une partition. On peut alors travailler avec les LVs comme avec des partitions classiques ([voir plus bas](#formater-et-monter-des-partitions)).

# Formater et monter des partitions

1. Formater une partition

```bash
$ mkfs -t <FS> <PARTITION>

# Par exemple
$ mkfs -t ext4 /dev/data/ma_data_frer
```

2. Monter une partition

```bash
$ mkdir <MOUNT_POINT>
$ mount <PARTITION> <MOUNT_POINT>

# Par exemple
$ mkdir /mnt/data1
$ mount /dev/data/ma_data_frer /mnt/data1

# Vérif 
$ mount # sans argument
$ df -h
```

3. Définir un montage automatique au boot de la machine

```bash
$ vim /etc/fstab
[...]
<PARTITION> <MOUNT_POINT> <FS> <OPTIONS> 0 0

# Par exemple
$ vim /etc/fstab
[...]
/dev/data/ma_data_frer /mnt/data1 ext4 defaults 0 0

# Vérif
$ sudo umount /mnt/data1 # démonter la partition si elle est déjà montée
$ sudo mount -av # remonter la partition, en utilisant les infos renseignées dans /etc/fstab
$ sudo reboot # non nécessaire si le mount -av fonctionne correctement
```

