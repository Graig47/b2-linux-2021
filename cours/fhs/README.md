# FHS : File Hierarchy Standard

*FHS* est le sigle pour *Filesystem Hierarchy Standard*. C'est un standard maintenu par la Linux Foundation. 

Il définit l'organisation et le rôle des dossiers et fichiers au sein d'un système d'exploitation. Beaucoup de systèmes UNIX, comme les distributions GNU/Linux, les distributions BSD ou MacOS ont adopté ce standard et le respectent plus ou moins scrupuleusement.

Il définit en particulier les dossiers à la racine du système de fichiers :

| Directory | Role                                                                                                                                                                                                      |
|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `bin`     | Exécutables, **bin**aires de la machine. Contient les commandes élémentaires du système comme `ls`, `cat`, `cp` etc                                                                                       |
| `boot`    | Fichiers nécessaires au **boot**. Notamment le noyau linux (fichier `vmlinuz-linux`) et un système de secours (rescue mode, fichier `initramfs-linux.img`)                                                |
| `dev`     | Périphériques ("**dev**ices") de la machine. Plus spécifiquement, on peut y trouver des fichiers qui représentent les différents périphériques de la machine, comme les disques durs                      |
| `etc`     | Fichiers de configuration du système et des applications                                                                                                                                                  |
| `home`    | Répertoires personnels des utilisateurs                                                                                                                                                                   |
| `lib`     | Librairies (32 bits) du système                                                                                                                                                                           |
| `lib64`   | Librairies (64 bits) du système                                                                                                                                                                           |
| `media`   | Contient les points de montage des périphériques amovibles                                                                                                                                                |
| `mnt`     | Points de montages des périphériques permanents                                                                                                                                                           |
| `opt`     | Répertoire contenant des applications. Libre à l'administrateur d'y stocker ce qu'il veut                                                                                                                 |
| `proc`    | Pseudo-répertoire qui représente l'état en temps réel de lu noyau/de l'OS                                                                                                                                 |
| `root`    | Répertoire personnel de l'utilisateur `root`                                                                                                                                                              |
| `run`     | Données liées au **run**time des applications (fichier de PID, de lock, etc)                                                                                                                              |
| `sbin`    | Exécutables, **bin**aires de la machine. Contient les commandes ajoutées au système par les utilisateurs.                                                                                                 |
| `srv`     | Libre à l'administrateur d'y stocker ce qu'il veut, généralement réservé à des données d'administration (répertoire de backups, dump de bases de données, etc.)                                           |
| `sys`     | Pseudo-répertoire qui représente l'état en temps réel de lu noyau/de l'OS                                                                                                                                 |
| `tmp`     | Dossier temporaire qui est vidée à chaque reboot. Beaucoup d'applications ont besoin de la présence d'un tel dossier. Attention, ses permissions sont extrêmement ouvertes (`777`)                        |
| `usr`     | Contient des dossiers similaires à ceux de la racine (`/usr/bin` par exemple) mais spécifique aux utilisateurs de la machines. `/usr/bin` contient les applications/binaires des utilisateurs par exemple |
| `var`     | Donneés des applications en cours de fonctionnement. Par exemple, une base de données stocke ses données là-bas.                                                                                          |




