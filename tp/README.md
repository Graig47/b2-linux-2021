# TP

- [TP1 : (re)Familiarisation avec un système GNU/Linux](./1)
- [TP2 pt. 1 : Gestion de service](./2)

## Aborder les TP

Le but des TP est d'explorer par la pratique les notions abordées en cours.

**Prenez votre temps.** L'idée est bien sûr d'arriver au bout du TP, mais vous devez toujours préférer comprendre les choses, plutôt que de vous hâter. Quitte à rendre un TP incomplet.

**Soyez avides, curieux.** Si vous souhaitez dériver du sujet de TP principal, pour bosser sur un truc qui vous concerne +, et qui reste en relation avec le TP, proposez moi ça.

**N'hésitez pas à me contacter.** IRL ou via Discord, je me rendrai dispo autant que possible.

**Lisez en entier les parties des TPs** avant de commencer à faire des bails. Ca donne souvent une meilleure idée de l'objectif attendu.

**Posez un max de questions.** Il n'y a pas de questions cons. Enfin, si. Mais la seule façon de savoir qu'elles sont cons, c'est de les poser :D

**Posez aussi vos questions à Google. EN ANGLAIS SVP.** En anglais, les résultats sont toujours plus nombreux et souvent plus pertinents.

## Rendu de TP

- format **Markdown**
- à travers une **plateforme git** accessible publiquement (comme Gitlab).
- soyez complets et clairs
  - souvent un copier/coller de la ligne de commande et une petite ligne d'explication suffisent
- sachez aussi être concis
  - ça m'intéresse pas forcément d'avoir TOUTES les étapes de vos manipulations sauf quand explicitement demandé
- **les éléments précédés de l'emoji 🌞 doivent absolument figurer dans le rendu**. Si c'est dans une bullet-list, tous les éléments doivent être rendus. Dans l'exemple qui suit, détail 1 et détail 2 doivent aussi figurer sur le rendu :

```markdown
- 🌞 Objectif global
  - détail 1
  - détail 2
```

- **les éléments précédés de l'emoji 📁** sont des fichiers qui doivent être dans le dépôt git de rendu

- **les TPs sont à rendre *via* MP sur discord avec la chaîne `NOM,URL`**
  - c'est le nom pas le prénom
  - l'`URL` doit pointer vers votre rendu directement, ou il doit être simple à trouver (une page avec tous les rendus et des liens vers les rendus c'est OK du moment que c'est clair)
  - si la chaîne `NOM,URL` est mal formatée je prends pas, vous êtes grands now
  - **si vous êtes en groupe, un seul rendu par groupe** (sauf si explicitement précisé un rendu individuel)
    - mettez un fichier `members` à côté du rendu de TP (sans extension de préférence), pour chaque TP
    - le fichier contient un NOM par ligne (et c'est tout)
    - par exemple :

```markdown
trump
wayne
```
